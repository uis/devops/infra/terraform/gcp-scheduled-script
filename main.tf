# kics-scan disable=d6cabc3a-d57e-48c2-b341-bf3dd4f4a120
# The first line in this file is to suppress "Cloud storage bucket should have logging enabled" in kics report.

# main.tf defines resources for the module

# The google_project data source is used to get the default projects for the
# Google providers.
data "google_project" "provider_project" {}

locals {
  # Allow the project and monitoring_project variables to override the provider project.
  project            = coalesce(var.project, data.google_project.provider_project.project_id)
  monitoring_project = coalesce(var.monitoring_project, data.google_project.provider_project.project_id)

  # A kebab-case name in case we're given underscore_case.
  kebab_name = replace(var.name, "_", "-")

  notification_channels = concat([
    for channel in google_monitoring_notification_channel.email : channel.id
  ], var.alert_notification_channels)
}

# The source code of the script.
data "archive_file" "source" {
  type = "zip"

  output_path = "${var.local_files_dir}/scripts/${var.name}-source.zip"

  source {
    content  = var.requirements
    filename = "requirements.txt"
  }

  source {
    content  = var.script
    filename = "main.py"
  }
}

# The source code lives as an object in a dedicated bucket.
resource "random_id" "source_bucket_name" {
  byte_length = 4
  prefix      = "${local.kebab_name}-source-"
}

# We don't use customer managed keys and, since this is a transient storage bucket, that's OK.
# trivy:ignore:AVD-GCP-0066
resource "google_storage_bucket" "source" {
  project = local.project
  name    = random_id.source_bucket_name.hex

  uniform_bucket_level_access = true

  versioning {
    enabled = var.enable_versioning
  }
  dynamic "lifecycle_rule" {
    for_each = var.enable_versioning == true ? [1] : []
    content {
      condition {
        age                = 1
        with_state         = "ANY"
        num_newer_versions = var.keep_versions
      }

      action {
        type = "Delete"
      }
    }
  }
  location = var.region
}

resource "google_storage_bucket_object" "source" {
  name   = "source-${data.archive_file.source.output_base64sha256}.zip"
  bucket = google_storage_bucket.source.name
  source = data.archive_file.source.output_path
}

# A service account which is used to run the script within a Cloud Function.
resource "google_service_account" "script" {
  count        = var.service_account == null ? 1 : 0
  project      = local.project
  account_id   = "script-${local.kebab_name}"
  display_name = "${var.name} script identity"
}

locals {
  # Convenience local for service account email that comes from either var or resource
  service_account_email = var.service_account == null ? google_service_account.script[0].email : var.service_account.email
}

# A Secret Manager secret which holds configuration for the script.
module "secret_configuration" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-secret-manager/devops"
  version = "< 5.0.0"

  project     = local.project
  region      = var.region
  secret_id   = "${local.kebab_name}-configuration"
  secret_data = var.secret_configuration
}

# Previously we used gen 1 functions. We can't directly upgrade a gen 1 to gen 2 function with the
# same name since Terraform doesn't know that it should completely remove the gen 1 resource before
# creating the gen 2 one. So, instead, we create a random id for the function based on the name.
resource "random_id" "script" {
  prefix      = "${var.name}-"
  byte_length = 4
}

# The script itself as a Cloud Function.
resource "google_cloudfunctions2_function" "script" {
  project  = local.project
  location = var.region

  name        = random_id.script.hex
  description = var.description

  build_config {
    runtime     = var.runtime
    entry_point = var.entry_point

    source {
      storage_source {
        bucket = google_storage_bucket.source.name
        object = google_storage_bucket_object.source.name
      }
    }
  }

  service_config {
    timeout_seconds       = var.timeout
    service_account_email = local.service_account_email
    available_cpu         = var.available_cpu
    available_memory      = "${var.available_memory_mb}Mi"
    max_instance_count    = 1

    environment_variables = merge(
      { SECRET_CONFIGURATION_SOURCE = module.secret_configuration.url },
      var.environment_variables
    )
  }

  lifecycle {
    ignore_changes = [
      # Cloud Functions will create a docker image repository for us. Don't try to keep removing it.
      build_config[0].docker_repository,
    ]
  }
}

# Allow the service account to invoke the function.
resource "google_cloudfunctions2_function_iam_member" "function_invoker" {
  project        = google_cloudfunctions2_function.script.project
  location       = google_cloudfunctions2_function.script.location
  cloud_function = google_cloudfunctions2_function.script.name

  role   = "roles/cloudfunctions.invoker"
  member = "serviceAccount:${local.service_account_email}"
}

resource "google_cloud_run_service_iam_member" "function_invoker" {
  project  = google_cloudfunctions2_function.script.project
  location = google_cloudfunctions2_function.script.location
  service  = google_cloudfunctions2_function.script.name

  role   = "roles/run.invoker"
  member = "serviceAccount:${local.service_account_email}"
}

# Allow the service account to read the configuration secret.
resource "google_secret_manager_secret_iam_member" "secret_reader" {
  project   = module.secret_configuration.project
  secret_id = module.secret_configuration.secret_id

  role   = "roles/secretmanager.secretAccessor"
  member = "serviceAccount:${local.service_account_email}"
}

# A scheduled job to run the script
resource "google_cloud_scheduler_job" "script" {
  project = local.project
  region  = var.region

  name             = var.name
  description      = var.description
  paused           = var.paused
  schedule         = var.schedule
  time_zone        = var.time_zone
  attempt_deadline = "${var.timeout}s"

  retry_config {
    retry_count          = var.schedule_retry_config.retry_count
    max_retry_duration   = var.schedule_retry_config.max_retry_duration
    min_backoff_duration = var.schedule_retry_config.min_backoff_duration
    max_backoff_duration = var.schedule_retry_config.max_backoff_duration
    max_doublings        = var.schedule_retry_config.max_doublings
  }

  http_target {
    http_method = "POST"
    uri         = google_cloudfunctions2_function.script.service_config[0].uri
    body        = base64encode("{}")

    oidc_token {
      audience              = "${google_cloudfunctions2_function.script.service_config[0].uri}/"
      service_account_email = local.service_account_email
    }
  }
}

# Email notification channels for alerts.
resource "google_monitoring_notification_channel" "email" {
  for_each = toset(var.alert_email_addresses)

  project = local.monitoring_project

  display_name = each.value
  type         = "email"

  labels = {
    email_address = each.value
  }
}

# A custom log metric used to select successful invocation of the script via the
# Cloud Scheduler job. Logging metrics can live in the project hosting logs
# since, counter-intuitively, it is not a Cloud Monitoring resource.
resource "google_logging_metric" "successes" {
  project = local.project

  name = "script/successes-${local.kebab_name}"

  filter = trimspace(replace(
    <<-EOI
      resource.type="cloud_scheduler_job"
      AND resource.labels.project_id = "${local.project}"
      AND resource.labels.job_id = "${google_cloud_scheduler_job.script.name}"
      AND httpRequest.status=200
    EOI
  , "\n", " "))

  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
  }
}

# Alerting if the time between successful POST-s to the script endpoint is
# greater than a threshold.
resource "google_monitoring_alert_policy" "successes" {
  project = local.monitoring_project

  display_name = "Invocations of ${var.name} script succeeds (${terraform.workspace})"

  enabled = var.alert_enabled

  notification_channels = local.notification_channels

  combiner = "OR"

  conditions {
    # Include the workspace name in the alert text to aid triaging alerts.
    display_name = "Invocations of ${var.name} script succeeds (${terraform.workspace})"

    condition_threshold {
      # We're interested in Cloud Scheduler successes for the job in the
      # appropriate project.
      filter = trimspace(replace(
        <<-EOI
          metric.type="logging.googleapis.com/user/${google_logging_metric.successes.id}"
          AND resource.type="cloud_scheduler_job"
          AND resource.label.project_id="${local.project}"
        EOI
        , "\n", " "
      ))

      # We check our threshold value every minute.
      duration   = "60s"
      comparison = "COMPARISON_LT"

      # The metric value is the number of successes in a given period.
      aggregations {
        alignment_period     = var.alert_success_period
        per_series_aligner   = "ALIGN_SUM"
        cross_series_reducer = "REDUCE_SUM"
      }

      threshold_value = var.alert_success_threshold
    }
  }
}


# A custom log metric used to select unsuccessful invocations of the script via the
# Cloud Scheduler job.
resource "google_logging_metric" "failures" {
  project = local.project

  name = "script/failures-${local.kebab_name}"

  filter = trimspace(replace(
    <<-EOI
      resource.type="cloud_scheduler_job"
      AND resource.labels.project_id = "${local.project}"
      AND resource.labels.job_id = "${google_cloud_scheduler_job.script.name}"
      AND httpRequest.status>=400
    EOI
  , "\n", " "))

  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
  }
}

# Alerting if a script failure is detected.
resource "google_monitoring_alert_policy" "failures" {
  project = local.monitoring_project

  display_name = "Invocations of ${var.name} script failed (${terraform.workspace})"

  enabled = var.failure_alert_enabled

  notification_channels = local.notification_channels

  combiner = "OR"

  conditions {
    # Include the workspace name in the alert text to aid triaging alerts.
    display_name = "Invocations of ${var.name} script failed (${terraform.workspace})"

    condition_threshold {
      # We're interested in Cloud Scheduler failures for the job in the
      # appropriate project.
      filter = trimspace(replace(
        <<-EOI
          metric.type="logging.googleapis.com/user/${google_logging_metric.failures.id}"
          AND resource.type="cloud_scheduler_job"
          AND resource.label.project_id="${local.project}"
        EOI
        , "\n", " "
      ))

      # We check our threshold value every minute.
      duration   = "60s"
      comparison = "COMPARISON_GT"

      # The metric value is the number of failures in a given period.
      aggregations {
        alignment_period     = var.alert_failure_period
        per_series_aligner   = "ALIGN_SUM"
        cross_series_reducer = "REDUCE_SUM"
      }

      threshold_value = var.alert_failure_threshold
    }
  }
}
