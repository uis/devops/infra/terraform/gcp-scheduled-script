# versions.tf defines minimum provider versions for the module

terraform {
  # Minimum provider versions.
  required_providers {
    archive = {
      source  = "hashicorp/archive"
      version = ">= 2.0"
    }
    google = {
      source  = "hashicorp/google"
      version = ">= 5.0"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 3.0"
    }
  }

  # Minimum terraform version.
  required_version = ">= 1.4"
}
