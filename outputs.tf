# outputs.tf describes the outputs from the module

output "function" {
  value = google_cloudfunctions2_function.script

  description = <<-EOT
    google_cloudfunctions2_function resource for the Cloud Function created to
    run the script.
  EOT
}

output "service_account" {
  # This is a bit ugly but its the only way to ensure that the type of the service_account output
  # will match the type of the service_account variable.
  value = var.service_account == null ? {
    account_id   = google_service_account.script[0].account_id
    display_name = google_service_account.script[0].display_name
    description  = google_service_account.script[0].description
    disabled     = google_service_account.script[0].disabled
    project      = google_service_account.script[0].project
    id           = google_service_account.script[0].id
    email        = google_service_account.script[0].email
    name         = google_service_account.script[0].name
    unique_id    = google_service_account.script[0].unique_id
    member       = google_service_account.script[0].member
  } : var.service_account

  description = <<-EOT
    google_service_account resource for the Service Account which the script
    runs as.
  EOT
}

output "job" {
  value = google_cloud_scheduler_job.script

  description = <<-EOT
    google_cloud_scheduler_job resource for the Scheduled Job which runs the script.
  EOT
}
