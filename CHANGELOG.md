# Changelog

## [4.0.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/compare/4.0.0...4.0.1) (2024-09-17)


### Bug Fixes

* update terraform docs ([5f05128](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/commit/5f051288fc785fabab4d2efeef8e32f24db6d27f))
* **versions:** be more broad in the versions we accept ([6266033](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/commit/626603320810dae2431cb6571a73fcad6db1a60b))

## [4.0.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/compare/3.4.2...4.0.0) (2024-09-06)


### ⚠ BREAKING CHANGES

* remove separate monitoring provider configuration

### Features

* remove separate monitoring provider configuration ([f782261](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/commit/f78226184df3e7aefd8fd28619f9bb630e2bf5af))

## [3.4.2](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/compare/3.4.1...3.4.2) (2024-08-20)

## [3.4.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/compare/3.4.0...3.4.1) (2024-08-20)

## [3.4.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/compare/3.3.0...3.4.0) (2024-08-19)


### Features

* add `schedule_retry_config` variable ([dfa8af2](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/commit/dfa8af286b994e4d2632dbb2b140121ee037e5ae)), closes [#15](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/issues/15)

## [3.3.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/compare/3.2.0...3.3.0) (2024-08-15)


### Features

* add `paused` variable for pausing/unpausing the schedule ([44d6841](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/commit/44d684141205277be8c530cb3e0c5031b200ebf6)), closes [#16](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/issues/16)

## [3.2.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/compare/3.1.3...3.2.0) (2024-08-14)


### Features

* add `available_cpu` variable ([1dd59d6](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/commit/1dd59d6265451acac29296bac88e44c9ad9c6243)), closes [#14](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/issues/14)

## [3.1.3](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/compare/3.1.2...3.1.3) (2024-07-24)

## [3.1.2](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/compare/3.1.1...3.1.2) (2024-07-08)

## [3.1.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/compare/3.1.0...3.1.1) (2024-07-04)


### Bug Fixes

* ignore changes to docker repository ([2f21ddc](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/commit/2f21ddc8a634dd4bd4ff899609f28cc5223ef64e))

## [3.1.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/compare/3.0.0...3.1.0) (2024-07-02)


### Features

* enable setting environment variables on the function ([a728822](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/commit/a728822ad941c3b46ab16520fb19fcc470a7aeeb))


### Bug Fixes

* disable markdownlint-cli2 job ([169a9d5](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/commit/169a9d5504e2a85159ad78cadce9860e7cc5fbc0))
* empty tuple when using a custom service account ([60da3d2](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/commit/60da3d2081d487b257d012ffcf2141336834f12d))

## [3.0.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/compare/2.4.0...3.0.0) (2024-06-20)


### ⚠ BREAKING CHANGES

* switch to using gen 2 Cloud Functions
* update pre-commit, CI jobs, docs and release configuration

### Features

* switch to using gen 2 Cloud Functions ([252441f](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/commit/252441f741b1c8a5acc36ad4d7f388b5534824b8))
* update pre-commit, CI jobs, docs and release configuration ([e947c0b](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-scheduled-script/commit/e947c0bf1d7aa5980231b844fcc9fa6b007c918a))

## [2.4.0] - 2024-05-29

### Added

- Added support for explicit failure alerts, enabled via the `failure_alert_enabled` variable and configurable
  via the `alert_failure_threshold` and `alert_failure_period` variables.

## [2.3.1] - 2024-02-05

### Added

- Variable `enable_versioning` added. If set to `true`, versioning is fully enabled for `source` bucket.
  By default versioning is enabled. To retain the old behavior, explicitly set this parameter to `false`.
- Variable `keep_versions` added. The number of versions to keep if `enable_versioning` is set to `true`. Default is 1.

## [2.3.0] - 2024-01-03

### Added

* Variable `alert_notification_channels` added, the old `alert_email_addresses` one
is now deprecated

## [2.2.1] - 2023-11-02

### Added

* Added support to publish to GitLab Terraform registry when tagged using semver

## [2.1.0] - 2021-09-08
### Added
 - Variable for optionally specifying existing service account to run function as

## [2.0.0] - 2021-09-07
### Changed
 - Broke compatibility with terraform 0.13 to allow 1.0 compatibility.

## [1.1.0] - 2021-09-07
### Added
 - Variable for optionally specifying existing service account to run function as

## [1.0.0] - 2021-03-16
### Added
 - Initial version
