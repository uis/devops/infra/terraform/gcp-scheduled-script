# variables.tf defines variables used by the module

variable "name" {
  type        = string
  description = "Short resource-friendly name for the script."
}

variable "local_files_dir" {
  type        = string
  description = <<-EOT
    A local directory where files may be created which persist between runs but
    which are not checked into source control.
  EOT
}

variable "description" {
  type        = string
  default     = ""
  description = "Longer human-friendly description of the script"
}

variable "project" {
  type        = string
  default     = ""
  description = "Project to create resources in. Defaults to provider project."
}

variable "monitoring_project" {
  type        = string
  default     = ""
  description = <<-EOT
    Project to create Cloud Monitoring resources in. Defaults to provider project.
  EOT
}

variable "region" {
  type        = string
  default     = "europe-west2"
  description = "Region to create resources in. Defaults to London, UK."
}

variable "paused" {
  type        = bool
  default     = false
  description = <<-EOT
    Flag indicating if the schedule is in a paused state or not. The script
    runs automatically according to the schedule when the schedule is not
    paused. When paused, the script only runs when triggered manually.
    Defaults to `true`.
  EOT
}

variable "script" {
  type        = string
  default     = <<-EOT
    def main(request):
        return 'ok'
  EOT
  description = <<-EOT
    The script to run. This should match the requirements for the Cloud Function
    Python runtime. The entrypoint defaults to "main" unless overridden via the
    "entry_point" variable.
  EOT
}

variable "entry_point" {
  type        = string
  default     = "main"
  description = "Entrypoint to script. Defaults to 'main'."
}

variable "requirements" {
  type        = string
  default     = ""
  description = "requirements.txt-style file containing script dependencies"
}

variable "schedule" {
  type        = string
  default     = "*/30 * * * *"
  description = <<-EOT
    Schedule for script running. Defaults to twice per hour. See
    https://cloud.google.com/scheduler/docs/configuring/cron-job-schedules for a
    description of the format.
  EOT
}

variable "time_zone" {
  type        = string
  default     = "Europe/London"
  description = <<-EOT
    Time zone which "schedule" should be evaluated in. Default: 'Europe/London'.
  EOT
}

variable "timeout" {
  type        = number
  default     = 120
  description = <<-EOT
    Maximum time, in seconds, that a script can take to execute. Invocations
    which take longer than this fail. Default: 120 seconds.
  EOT

  validation {
    condition     = var.timeout > 0 && var.timeout <= 3600
    error_message = "The `timeout` value must be > 0 and <= 3600 seconds."
  }
}

variable "available_cpu" {
  type        = number
  description = <<-EOT
    Maxiumum number of CPUs available to the script. See
    https://cloud.google.com/functions/docs/configuring/memory for valid
    values.

    When this value is set, `available_memory_mb` must also be specified.
    Otherwise, the default is based on the value of `available_memory_mb`.
  EOT

  validation {
    condition = contains(
      [0.083, 0.167, 0.333, 0.583, 1, 2, 4, 8], var.available_cpu
    )
    error_message = <<-EOT
      The `available_cpu` value must be valid. See
      https://cloud.google.com/functions/docs/configuring/memory
    EOT
  }
}

variable "schedule_retry_config" {
  type = object({
    retry_count          = optional(number, 1)
    max_retry_duration   = optional(string)
    min_backoff_duration = optional(string)
    max_backoff_duration = optional(string)
    max_doublings        = optional(number)
  })
  default     = {}
  description = <<-EOT
    If a scheduled job does not complete successfully, meaning that an
    acknowledgement is not received from the handler, then it will be retried
    with exponential backoff according to these configuration options. See
    https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_scheduler_job#nested_retry_config
    for more information about these options.
  EOT

  validation {
    condition = (
      var.schedule_retry_config.retry_count >= 0 &&
      var.schedule_retry_config.retry_count <= 5
    )
    error_message = <<-EOT
      The `schedule_retry_config.retry_count` value must be >= 0 and
      <= 5.
    EOT
  }

  validation {
    condition = (
      var.schedule_retry_config.max_retry_duration == null ||
      can(regex(
        "^\\d+(\\.\\d{1,9})?s$",
        var.schedule_retry_config.max_retry_duration
      ))
    )
    error_message = <<-EOT
      The `schedule_retry_config.max_retry_duration` value must be a string
      containing a numnber with up to nine fractional digits, terminated by
      's'.
    EOT
  }
  validation {
    condition = (
      var.schedule_retry_config.min_backoff_duration == null ||
      can(regex(
        "^\\d+(\\.\\d{1,9})?s$",
        var.schedule_retry_config.min_backoff_duration
      ))
    )
    error_message = <<-EOT
      The `schedule_retry_config.min_backoff_duration` value must be a string
      containing a numnber with up to nine fractional digits, terminated by
      's'.
    EOT
  }
  validation {
    condition = (
      var.schedule_retry_config.max_backoff_duration == null ||
      can(regex(
        "^\\d+(\\.\\d{1,9})?s$",
        var.schedule_retry_config.max_backoff_duration
      ))
    )
    error_message = <<-EOT
      The `schedule_retry_config.max_backoff_duration` value must be a string
      containing a numnber with up to nine fractional digits, terminated by
      's'.
    EOT
  }
}

variable "available_memory_mb" {
  type        = number
  default     = 128
  description = <<-EOT
    Maxiumum memory available to the script in MiB. See
    https://cloud.google.com/functions/docs/configuring/memory for valid
    values. Default: 128 MiB.
  EOT

  validation {
    condition = contains(
      [128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768],
      var.available_memory_mb
    )
    error_message = <<-EOT
      The `available_memory_mb` value must be valid. See
      https://cloud.google.com/functions/docs/configuring/memory
    EOT
  }
}

variable "alert_email_addresses" {
  type        = list(string)
  default     = []
  description = <<EOL
    DEPRECATED. An optional list of email addresses which should recieve alerts.
    This creates a separate Notification Channel for every email address in the list which is often not desired.
    Consider creating a Notification Channel separately and passing its ID to the alert_notification_channels
    variable instead.
  EOL
}

variable "alert_notification_channels" {
  type        = list(string)
  default     = []
  description = <<EOL
    A list of notification channel IDs to send alerts to. The format for the channel IDs should
    be as follows.

    [
      "projects/[PROJECT_ID]/notificationChannels/[CHANNEL_ID]"
    ]
  EOL
}

variable "alert_success_threshold" {
  type        = number
  default     = 5
  description = <<-EOT
    The minimum number of successes within 'alert_success_period' below which an
    alert is fired.
  EOT
}

variable "alert_success_period" {
  type        = string
  default     = "3600s"
  description = <<-EOT
    Period over which 'alert_success_threshold' is used. Default: "3600s" which
    is one hour.
  EOT
}

variable "alert_enabled" {
  type        = bool
  default     = true
  description = <<-EOT
    Flag indicating if alerting should be enabled for this script.
  EOT
}

variable "failure_alert_enabled" {
  type        = bool
  default     = false
  description = <<-EOT
    Flag indicating if failure alerts should be enabled for this script, separate
    to the default alert which is raised if a successful invocation has not been
    detected within the alerting period.
  EOT
}

variable "alert_failure_threshold" {
  type        = number
  default     = 1
  description = <<-EOT
    The number of failures which will trigger a failure alert within 'alert_success_period'.
  EOT
}

variable "alert_failure_period" {
  type        = string
  default     = "3600s"
  description = <<-EOT
    Period over which 'alert_failure_threshold' is used.
  EOT
}

variable "secret_configuration" {
  type        = string
  default     = ""
  description = <<-EOT
    Configuration which is placed in a Google Secret which can be read by the
    service account identity which the script runs as. A path to the secret of
    the form "sm://[PROJECT]/[SECRET]#[VERSION]" appears as the
    "SECRET_CONFIGURATION_SOURCE" environment variable.
  EOT
}

variable "runtime" {
  type        = string
  default     = "python38"
  description = <<-EOT
    Python runtime for script. See
    https://cloud.google.com/functions/docs/concepts/exec. Default: "python38".
  EOT
}

variable "service_account" {
  type = object({
    account_id   = string
    display_name = optional(string)
    description  = optional(string)
    disabled     = optional(bool)
    project      = optional(string)
    id           = string
    email        = string
    name         = string
    unique_id    = string
    member       = string
  })
  default     = null
  description = <<-EOT
    Optional existing service account to run script as. If one is not provided
    then one is created. Either way, the relevant permissions will be given to
    the account.

    Note: one needs to pass the actual service account *object* here which will be
    mirrored to the `service_account` output.
  EOT
}

variable "enable_versioning" {
  description = <<-EOT
    The bucket's versioning configuration.
    While set to true, versioning is fully enabled for the bucket.
  EOT
  type        = bool
  default     = true
}

variable "keep_versions" {
  description = <<-EOT
    The number of file versions to keep if enable_versioning is set to true.
    Default: 1
  EOT
  type        = number
  default     = 1
}

variable "environment_variables" {
  description = "Additional environment variables to set on the function."
  default     = {}
  type        = map(string)
}
