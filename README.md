<!-- BEGIN_TF_DOCS -->
# Scheduled script run within Google Cloud

This terraform module configures a single-file Python script to be run regularly
in Google Cloud using a cron-style scheduling syntax. It supports single-file
scripts with requirements specified via a `requirements.txt`-style list of
packages.

The scripts may have secrets passed to them by a Google Secret Manager secret
created for this script.

A dedicated service account is created for the script which represents the
identity which the script runs as within Google Cloud.

Monitoring is configured with alerts sent if the number of successful
invocations of the script within a configurable time period falls below a given
threshold.

This module is not suitable for multi-file scripts or scripts which need to be
passed parameters which differ from run-to-run.

## Versioning

The `master` branch contains the tip of development. Releases are made automatically
using [release automation](https://guidebook.devops.uis.cam.ac.uk/explanations/gitlab-release-automation/).

## Required APIs and services

The following APIs must be enabled to use this module:

* `appengine.googleapis.com`
* `cloudbuild.googleapis.com`
* `cloudfunctions.googleapis.com`
* `cloudscheduler.googleapis.com`
* `secretmanager.googleapis.com`

In addition there must be an app engine application configured for your project.
This is a requirement to configure Cloud Scheduler jobs. You can ensure an app
engine application is configured by means of the `google_app_engine_application`
resource:

```tf
resource "google_app_engine_application" "app" {
  location_id = "europe-west2"
}
```

## Monitoring

Cloud Monitoring resources are associated with a Monitoring Workspace which may
be hosted in a project which differs from the project containing other
resources. The `google.monitoring` provider is used when creating Monitoring
resources and it should be a provider configured with credentials able to create
monitoring resources.

## Implementation

The script is implemented as a Cloud Function and so it should conform to the
[Cloud Function Python
runtime](https://cloud.google.com/functions/docs/concepts/python-runtime). The
entrypoint to the function can be customised but the default is to use `main`.

A dedicated Cloud Storage bucket is created to store the source code in. This is
created with a random name.

## Example

A minimal example of running a script:

```tf
module "scheduledscript" {
  source = "..."

  # Name used to form resource names and human readable description.
  name        = "my-script"
  description = "Log a message."

  # Project and region to create resources in.
  project = "..."
  region  = "europe-west2"

  # A directory which can be used to store local files which are not checked in
  # to source control.
  local_files_dir = "/terraform_data/local"

  # The script itself.
  script = <<-EOL
      import logging

      LOG = logging.getLogger()
      logging.basicConfig(level=logging.INFO)

      def main(request):
          LOG.info('I ran!')
          return '{ "status": "ok" }', {'Content-Type': 'application/json'}
  EOL

  # Cron-style schedule for running the job.
  schedule = "*/5 * * * *"

  # Alerting threshold. Specify the minimum number of successful invocations and
  # the period over which this should be measured.
  alert_success_threshold = 5
  alert_success_period    = "3600s"  # == 1hr

  # Email addresses to receive alerts if invocations are failing.
  alert_email_addresses = ["someone@example.com"]

  # Use a separate provider with rights over the Cloud Monitoring workspace for
  # monitoring resource management.
  providers = {
    google.monitoring = google.monitoring
  }
}
```

See the [variables.tf](variables.tf) file for all variables.

Additional Python package requirements can be specified in the `requirements`
variable which should be a list of Python packages, one per line, as used by
`requirements.txt` files.

A custom entry point for the script can be configured via the `entry_point`
variable.

Configurations can be passed to the application via the `secret_configuration`
variable. This should be a string which is placed in a Google Secret Manager
secret and is made available to the script. The secret is passed as a URL of the
form `sm://[PROJECT]/[SECRET]#[VERSION]` in the `SECRET_CONFIGURATION_SOURCE`
environment variable.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | >= 2.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 5.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | >= 3.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_alert_email_addresses"></a> [alert\_email\_addresses](#input\_alert\_email\_addresses) | DEPRECATED. An optional list of email addresses which should recieve alerts.<br>    This creates a separate Notification Channel for every email address in the list which is often not desired.<br>    Consider creating a Notification Channel separately and passing its ID to the alert\_notification\_channels<br>    variable instead. | `list(string)` | `[]` | no |
| <a name="input_alert_enabled"></a> [alert\_enabled](#input\_alert\_enabled) | Flag indicating if alerting should be enabled for this script. | `bool` | `true` | no |
| <a name="input_alert_failure_period"></a> [alert\_failure\_period](#input\_alert\_failure\_period) | Period over which 'alert\_failure\_threshold' is used. | `string` | `"3600s"` | no |
| <a name="input_alert_failure_threshold"></a> [alert\_failure\_threshold](#input\_alert\_failure\_threshold) | The number of failures which will trigger a failure alert within 'alert\_success\_period'. | `number` | `1` | no |
| <a name="input_alert_notification_channels"></a> [alert\_notification\_channels](#input\_alert\_notification\_channels) | A list of notification channel IDs to send alerts to. The format for the channel IDs should<br>    be as follows.<br><br>    [<br>      "projects/[PROJECT\_ID]/notificationChannels/[CHANNEL\_ID]"<br>    ] | `list(string)` | `[]` | no |
| <a name="input_alert_success_period"></a> [alert\_success\_period](#input\_alert\_success\_period) | Period over which 'alert\_success\_threshold' is used. Default: "3600s" which<br>is one hour. | `string` | `"3600s"` | no |
| <a name="input_alert_success_threshold"></a> [alert\_success\_threshold](#input\_alert\_success\_threshold) | The minimum number of successes within 'alert\_success\_period' below which an<br>alert is fired. | `number` | `5` | no |
| <a name="input_available_cpu"></a> [available\_cpu](#input\_available\_cpu) | Maxiumum number of CPUs available to the script. See<br>https://cloud.google.com/functions/docs/configuring/memory for valid<br>values.<br><br>When this value is set, `available_memory_mb` must also be specified.<br>Otherwise, the default is based on the value of `available_memory_mb`. | `number` | n/a | yes |
| <a name="input_available_memory_mb"></a> [available\_memory\_mb](#input\_available\_memory\_mb) | Maxiumum memory available to the script in MiB. See<br>https://cloud.google.com/functions/docs/configuring/memory for valid<br>values. Default: 128 MiB. | `number` | `128` | no |
| <a name="input_description"></a> [description](#input\_description) | Longer human-friendly description of the script | `string` | `""` | no |
| <a name="input_enable_versioning"></a> [enable\_versioning](#input\_enable\_versioning) | The bucket's versioning configuration.<br>While set to true, versioning is fully enabled for the bucket. | `bool` | `true` | no |
| <a name="input_entry_point"></a> [entry\_point](#input\_entry\_point) | Entrypoint to script. Defaults to 'main'. | `string` | `"main"` | no |
| <a name="input_environment_variables"></a> [environment\_variables](#input\_environment\_variables) | Additional environment variables to set on the function. | `map(string)` | `{}` | no |
| <a name="input_failure_alert_enabled"></a> [failure\_alert\_enabled](#input\_failure\_alert\_enabled) | Flag indicating if failure alerts should be enabled for this script, separate<br>to the default alert which is raised if a successful invocation has not been<br>detected within the alerting period. | `bool` | `false` | no |
| <a name="input_keep_versions"></a> [keep\_versions](#input\_keep\_versions) | The number of file versions to keep if enable\_versioning is set to true.<br>Default: 1 | `number` | `1` | no |
| <a name="input_local_files_dir"></a> [local\_files\_dir](#input\_local\_files\_dir) | A local directory where files may be created which persist between runs but<br>which are not checked into source control. | `string` | n/a | yes |
| <a name="input_monitoring_project"></a> [monitoring\_project](#input\_monitoring\_project) | Project to create Cloud Monitoring resources in. Defaults to provider project. | `string` | `""` | no |
| <a name="input_name"></a> [name](#input\_name) | Short resource-friendly name for the script. | `string` | n/a | yes |
| <a name="input_paused"></a> [paused](#input\_paused) | Flag indicating if the schedule is in a paused state or not. The script<br>runs automatically according to the schedule when the schedule is not<br>paused. When paused, the script only runs when triggered manually.<br>Defaults to `true`. | `bool` | `false` | no |
| <a name="input_project"></a> [project](#input\_project) | Project to create resources in. Defaults to provider project. | `string` | `""` | no |
| <a name="input_region"></a> [region](#input\_region) | Region to create resources in. Defaults to London, UK. | `string` | `"europe-west2"` | no |
| <a name="input_requirements"></a> [requirements](#input\_requirements) | requirements.txt-style file containing script dependencies | `string` | `""` | no |
| <a name="input_runtime"></a> [runtime](#input\_runtime) | Python runtime for script. See<br>https://cloud.google.com/functions/docs/concepts/exec. Default: "python38". | `string` | `"python38"` | no |
| <a name="input_schedule"></a> [schedule](#input\_schedule) | Schedule for script running. Defaults to twice per hour. See<br>https://cloud.google.com/scheduler/docs/configuring/cron-job-schedules for a<br>description of the format. | `string` | `"*/30 * * * *"` | no |
| <a name="input_schedule_retry_config"></a> [schedule\_retry\_config](#input\_schedule\_retry\_config) | If a scheduled job does not complete successfully, meaning that an<br>acknowledgement is not received from the handler, then it will be retried<br>with exponential backoff according to these configuration options. See<br>https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_scheduler_job#nested_retry_config<br>for more information about these options. | <pre>object({<br>    retry_count          = optional(number, 1)<br>    max_retry_duration   = optional(string)<br>    min_backoff_duration = optional(string)<br>    max_backoff_duration = optional(string)<br>    max_doublings        = optional(number)<br>  })</pre> | `{}` | no |
| <a name="input_script"></a> [script](#input\_script) | The script to run. This should match the requirements for the Cloud Function<br>Python runtime. The entrypoint defaults to "main" unless overridden via the<br>"entry\_point" variable. | `string` | `"def main(request):\n    return 'ok'\n"` | no |
| <a name="input_secret_configuration"></a> [secret\_configuration](#input\_secret\_configuration) | Configuration which is placed in a Google Secret which can be read by the<br>service account identity which the script runs as. A path to the secret of<br>the form "sm://[PROJECT]/[SECRET]#[VERSION]" appears as the<br>"SECRET\_CONFIGURATION\_SOURCE" environment variable. | `string` | `""` | no |
| <a name="input_service_account"></a> [service\_account](#input\_service\_account) | Optional existing service account to run script as. If one is not provided<br>then one is created. Either way, the relevant permissions will be given to<br>the account.<br><br>Note: one needs to pass the actual service account *object* here which will be<br>mirrored to the `service_account` output. | <pre>object({<br>    account_id   = string<br>    display_name = optional(string)<br>    description  = optional(string)<br>    disabled     = optional(bool)<br>    project      = optional(string)<br>    id           = string<br>    email        = string<br>    name         = string<br>    unique_id    = string<br>    member       = string<br>  })</pre> | `null` | no |
| <a name="input_time_zone"></a> [time\_zone](#input\_time\_zone) | Time zone which "schedule" should be evaluated in. Default: 'Europe/London'. | `string` | `"Europe/London"` | no |
| <a name="input_timeout"></a> [timeout](#input\_timeout) | Maximum time, in seconds, that a script can take to execute. Invocations<br>which take longer than this fail. Default: 120 seconds. | `number` | `120` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_function"></a> [function](#output\_function) | google\_cloudfunctions2\_function resource for the Cloud Function created to<br>run the script. |
| <a name="output_job"></a> [job](#output\_job) | google\_cloud\_scheduler\_job resource for the Scheduled Job which runs the script. |
| <a name="output_service_account"></a> [service\_account](#output\_service\_account) | google\_service\_account resource for the Service Account which the script<br>runs as. |
<!-- END_TF_DOCS -->
